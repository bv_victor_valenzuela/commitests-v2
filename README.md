
## Correr el proyecto

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Abrir [http://localhost:3000](http://localhost:3000) en el navegador.


## Probar los commits

Realice cualquier cambio en el proyecto

```bash
git add .
```

Intente guardar el commit de sus cambios

```bash
git commit -m "descripción de los cambios"
```

## Tipos

- **feat**: new feature for the user, not a new feature for build script
- **fix**: bug fix for the user, not a fix to a build script
- **docs**: changes to the documentation
- **style**: formatting, missing semi colons, etc; no production code change
- **refactor**: refactoring production code, eg. renaming a variable
- **test**: adding missing tests, refactoring tests; no production code change
- **chore**: updating grunt tasks etc; no production code change)
- **config**: configuration of linter, package, babel, etc



## Ejemplos

Se presentan ejemplos de commits


```bash
git commit -m "config(package.json): added config type to commit linter" 
git commit -m "fix(button): handled appropiate callback"
```

## Documentación de los packages

https://www.npmjs.com/package/husky

https://www.npmjs.com/package/@commitlint/config-conventional





