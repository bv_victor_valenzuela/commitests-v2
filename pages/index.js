import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { React, useState } from 'react';

export default function Home() {
  const [submitted, setFormSubmitted] = useState(false);
  return (
    <div className="App App-header">
      <Head>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
          crossorigin="anonymous"
        ></link>
      </Head>
      <h3 className="text-center p-3 bg-secondary text-white">
        Registration form
      </h3>
      {submitted ? (
        <div id="success" className="text-center m-5 fw-bold fs-3">
          Registration request submitted successfully
        </div>
      ) : (
        <div className="container text-center">
          <div className="row justify-content-center">
            <div className="col-sm-12 col-md-10 p-2">
              <input
                className="form-control"
                name="firstName"
                id="firstName"
                placeholder="First name"
              />
            </div>
            <div className="col-sm-12 col-md-10 p-2">
              <input
                className="form-control"
                name="lastName"
                id="lastName"
                placeholder="Last name"
              />
            </div>
            <div className="col-sm-12 col-md-10 p-2">
              <input
                className="form-control"
                name="email"
                id="email"
                type="email"
                placeholder="Email"
              />
            </div>
            <div className="col-sm-12 col-md-10 p-2">
              <input
                className="form-control"
                name="username"
                id="username"
                type="text"
                placeholder="User name"
              />
            </div>
            <div className="col-sm-12 col-md-10 p-2">
              <input
                className="form-control"
                name="password"
                id="password"
                type="password"
                placeholder="Password"
              />
            </div>
            <div className="col-sm-12 col-md-10 p-2">
              <button
                type="submit"
                id="submit"
                className="btn btn-outline-primary btn-lg"
                onClick={() => setFormSubmitted(true)}
              >
                Register
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
